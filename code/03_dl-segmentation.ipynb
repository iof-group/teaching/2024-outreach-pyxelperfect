{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5ec34004",
   "metadata": {},
   "source": [
    "## Part 4 - Deep Learning automatic segmentation\n",
    "This Python for image analysis workshop wouldn't be completed if we wouldn't show you a simple application of the most commonly used deep-learning segmentation methods. These can be often used out-of-the-box without complicated parameter tuning even in complicated imaging conditions.\n",
    "\n",
    "Here we will explore:\n",
    "- [StarDist](https://github.com/stardist/stardist)\n",
    "- [Cellpose](https://cellpose.readthedocs.io/en/latest/)\n",
    "\n",
    "Explaining the theory behind each model is besides the goal of this workshop, so please refer to the provided links to know more about their context, theory and applications."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2fe6cbbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Package import\n",
    "import os  # deal with OS-specific tasks: moving, renaming, deleting files\n",
    "\n",
    "import numpy as np  # the go-to package to deal with array data\n",
    "import stackview  # a new package to visually inspect images\n",
    "import tifffile  # the best package to read/write TIFFs\n",
    "from tqdm.auto import tqdm  # a nice package that creates a progress bar"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3010e617",
   "metadata": {},
   "source": [
    "With the examples below we focus on a **single image**, mostly because of time constrains. If you have more time, feel free to loop through all the images (between conditions and time frames)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ac61a711",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "47804c8f46b347c18db137772c6d85bf",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HBox(children=(VBox(children=(ImageWidget(height=520, width=696),)),)), HBox(children=(Button(d…"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Load a single image from specified position and frame number\n",
    "pos_id = 37\n",
    "frame_id = 1\n",
    "\n",
    "# Load RAW nuclei image\n",
    "nuc = tifffile.imread(\n",
    "    f\"../data/00{pos_id}/tubulin_P00{pos_id}_T00{frame_id:03d}_Crfp_Z1_S1.tif\"\n",
    ")\n",
    "# Load segmented image from earlier\n",
    "nuc_seg_trad = tifffile.imread(\n",
    "    f\"../results/00{pos_id}/nuclear_segmentation.tif\", key=frame_id - 1\n",
    ")\n",
    "\n",
    "# Visualize loaded images\n",
    "stackview.switch(\n",
    "    {\"Raw\": nuc, \"Trad segmentation\": nuc_seg_trad},\n",
    "    zoom_factor=0.5,\n",
    "    display_max=100,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9db4c18e",
   "metadata": {},
   "source": [
    "## Segmentation StarDist\n",
    "StarDist comes with severa pre-trained models available. Using the code cell below we can see which ones they are. To know more about them please see their [documentation](https://github.com/stardist/stardist?tab=readme-ov-file#pretrained-models-for-2d)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c0826f8b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "There are 4 registered models for 'StarDist2D':\n",
      "\n",
      "Name                  Alias(es)\n",
      "────                  ─────────\n",
      "'2D_versatile_fluo'   'Versatile (fluorescent nuclei)'\n",
      "'2D_versatile_he'     'Versatile (H&E nuclei)'\n",
      "'2D_paper_dsb2018'    'DSB 2018 (from StarDist 2D paper)'\n",
      "'2D_demo'             None\n"
     ]
    }
   ],
   "source": [
    "from stardist.models import StarDist2D\n",
    "\n",
    "StarDist2D.from_pretrained()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72279fb7",
   "metadata": {},
   "source": [
    "Versatile fluorescent nuclei model is, as the name suggests, very versatile! For the model to work properly we have to make sure to normalize our image (between 0 and 1), likely for us the [CSBDeep package](https://csbdeep.bioimagecomputing.com/) already comes with a function for that.\n",
    "\n",
    "Feel free to try and use some of the other models and compare their performances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "a58b9a4b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found model '2D_versatile_fluo' for 'StarDist2D'.\n",
      "Loading network weights from 'weights_best.h5'.\n",
      "Loading thresholds from 'thresholds.json'.\n",
      "Using default values: prob_thresh=0.479071, nms_thresh=0.3.\n"
     ]
    }
   ],
   "source": [
    "# StarDist works much better if values in the image are normalized\n",
    "from csbdeep.utils import normalize\n",
    "\n",
    "\n",
    "def stardist_predict(img, model):\n",
    "    \"\"\"\n",
    "    Normalize the image and use StarDist model to predict objects instances\n",
    "    \"\"\"\n",
    "    norm_img = normalize(img, 1, 99)\n",
    "    lbl, _ = model.predict_instances(norm_img)\n",
    "    return lbl\n",
    "\n",
    "# Create model object from pre-trained model\n",
    "model = StarDist2D.from_pretrained(\"2D_versatile_fluo\")\n",
    "# Predict instances --> return labeled objects\n",
    "nuc_stardist = stardist_predict(nuc, model)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "931a3235",
   "metadata": {},
   "source": [
    "Let's take a look at the results!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6aef02e5",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c6661fd6ea784e0697955d6d93347487",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HBox(children=(VBox(children=(ImageWidget(height=520, width=696),)),)), IntSlider(value=520, de…"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# use stackview like in previous notebooks\n",
    "stackview.curtain(\n",
    "    nuc,\n",
    "    nuc_stardist,\n",
    "    zoom_factor=0.5,\n",
    "    alpha=0.5,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1457262d",
   "metadata": {},
   "source": [
    "## Compare with our previous segmentation\n",
    "Let's compare the StarDist segmentation with the one we had performed in the previous notebooks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "93d2d22a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "d664b2e29dc443d2aaf47485b5b1af94",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HBox(children=(VBox(children=(ImageWidget(height=520, width=696),)),)), HBox(children=(Button(d…"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "stackview.switch(\n",
    "    {\n",
    "        \"Raw\": nuc,\n",
    "        \"Trad segmentation\": nuc_seg_trad,\n",
    "        \"StarDist segmentation\": nuc_stardist,\n",
    "    },\n",
    "    zoom_factor=0.5,\n",
    "    display_max=100,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1eae3b01",
   "metadata": {},
   "source": [
    "### Comparison metrics\n",
    "Simply looking by eyes can be useful for a few images when estimating segmentation precision, but for a more **quantitative and robust evaluation** a plethora of metrics exist to evaluate objective segmentation performance. \n",
    "\n",
    "Here we see some of them using a the `matching` function from the StarDist package itself. These are based on the [intersection over union metric (IoU)](https://viso.ai/computer-vision/intersection-over-union-iou/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "ceb32c2e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Matching(criterion='iou', thresh=0.5, fp=7, tp=165, fn=17, precision=0.9593023255813954, recall=0.9065934065934066, accuracy=0.873015873015873, f1=0.9322033898305084, n_true=182, n_pred=172, mean_true_score=0.8132034972473815, mean_matched_score=0.8969880999940815, panoptic_quality=0.8361753474521099)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from stardist.matching import matching\n",
    "\n",
    "matching(nuc_stardist, nuc_seg_trad)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ebd8beb",
   "metadata": {},
   "source": [
    "## Segmentation Cellpose\n",
    "Cellpose shines when segmenting all types of cellular-looking structures! So far we have only worked on the nuclear channel, let's try to segment also the tubulin channel of our data using a pre-trained model of Cellpose. As StarDist, Cellpose comes with different pre-trained models, check them out [here](https://cellpose.readthedocs.io/en/latest/models.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "d290ca1c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# As before, select one position and frame\n",
    "pos_id = 37\n",
    "frame_id = 1\n",
    "\n",
    "# Load RAW tubulin channel\n",
    "tub = tifffile.imread(\n",
    "    f\"../data/00{pos_id}/tubulin_P00{pos_id}_T00{frame_id:03d}_Cgfp_Z1_S1.tif\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "5631b749",
   "metadata": {},
   "outputs": [],
   "source": [
    "from cellpose import models\n",
    "\n",
    "# Create Cellpose model object\n",
    "model = models.Cellpose(model_type=\"cyto2\")\n",
    "# Predict instances --> return labeled objects (and more..)\n",
    "msk, flows, styles, diams = model.eval(tub, diameter=None, channels=[0, 0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "a1195e20",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "af4a620c136148ad863bf7f423083e25",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HBox(children=(VBox(children=(ImageWidget(height=520, width=696),)),)), IntSlider(value=520, de…"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Visualize segmentation results\n",
    "stackview.curtain(\n",
    "    tub,\n",
    "    msk.astype(\"uint32\"),\n",
    "    zoom_factor=0.5,\n",
    "    alpha=0.8,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "133b9a7f",
   "metadata": {},
   "source": [
    "Pretty impressive right?\n",
    "\n",
    "Feel free to implement a loop to use Cellpose to segment all frames from all movies, and why not calculate some features from the tubulin channel like we did for the nuclei to see if there are significant differences in experimental conditions? We haven't even touched on the topic of tracking cells in time, if you feel confident take a look at [Bayesian Tracker](https://btrack.readthedocs.io/en/latest/)!\n",
    "\n",
    "## Conclusions\n",
    "There is much more we could have gone through and explained in this workshop. This material is here for your own use, improve it and expand it as you want. In the end we are aiming for you to be comfortable with:\n",
    "- The use of Jupyter notebooks\n",
    "- Basics of reading and exporting images as TIF\n",
    "- Exploring your data via visualization and simple statistics\n",
    "- Basic creation and application of processing or analysis pipeline in a reproducible manner\n",
    "- Simple data exploration and figure creation ideas"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
