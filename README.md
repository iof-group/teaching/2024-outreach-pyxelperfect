# Pyxel perfect 2024
## A Jupyter introductory journey for image analysis

Introductory workshop for the use of Jupyter notebooks for image analysis pipelines by the Imaging and Optics Facility (IOF) team at IST Austria.

As Image analysis is a fast developing field, we feel the need to show you how to use Jupyter notebooks with a broadly applicable Python pipeline for basic image analysis tasks (displaying and investigating images, segmentation and some feature extraction with simple plotting). We will quickly introduce some theory and navigate together through the notebooks we created for you before leaving some space for your own questions and data or exercises.

This workshop is targeted for semi-advanced users and some basic knowledge of automation and coding is required. This is not intended to be a Python introduction for beginners and we will try to tailor the content of workshop to the signed-up users. We are offering 10 spots for on-site participants and will ask you to fill in a small questionnaire. There is no need to have any of your own data, however we would like to highly encourage you to bring some and ask any questions! Only thing that you need is a laptop, enthusiasm and good mood.

In case of any questions, feel free to contact us at [iof@ista.ac.at](iof@ista.ac.at).

## Schedule
|      Time     |                  What                 |
|:-------------:|---------------------------------------|
| 13:00 - 13:30 | Image processing theory               |
| 13:30 - 13:45 | Intro to Jupyter                      |
| 13:45 - 15:30 | Exploring data and basic segmentation |
| 15:30 - 15:45 | Break                                 |
| 15:45 - 16:30 | Advanced segmentation and plotting    |
| 16:30 - 17:00 | Q&A and work on your data             |

## Outline

```bash
REPOSITORY
├── LICENSE
├── README.md
├── code <- workshop code content
│   ├── 00_the-data.ipynb
│   ├── 01_segmentation.ipynb
│   ├── 02_features-plotting.ipynb
│   └── 03_dl-segmentation.ipynb
├── data <- content git ignored, data auto-download
├── presentation <- workshop slides
|   ├── image_analysis_theory
|   └── intro_jupyter
└── requirements.txt
```

## Data

### Chromatin + Microtubles
[**H2B-mCherry (red) + mEGFP-α-Tubulin (green)**](https://cellcognition-project.org/demo_data.html)

Human Hela Kyoto cells stably expressing fluorescent markers of chromatin (histone-2b, H2B) and alpha-Tubulin. Data acquired by Katja Beck (Kutay group, ETH Zurich) and Michael Held (Gerlich group, ETH Zurich) at a Molecular Devices ImageXpress Micro.

Widefield epifluorescence, 20x dry objective (0.3225 µm / pixel), 1392x1040 pixel, 206 frames x 4.6 min = 15.8 h

## Installation
1. Install required packages from requirements.txt
2. Start Jupyterlab instance
3. Navigate notebooks in code directory

## Authors, contributors and teachers
For questions or feedback please contact our general mailing list: [iof@ist.ac.at](iof@ist.ac.at)

Marco Dalla Vecchia

Christopher Sommer

Tereza Belinova

Bernhard Hochreiter

## License and usage

The content of this workshop is publicly available: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by the Free Software Foundation 
in version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
Public License v3 for more details.

You should have received a copy of the GNU General Public License v3 along with this program. 
If not, see <https://www.gnu.org/licenses/>.

Contact the Technology Transfer Office, ISTA, Am Campus 1, A-3400 Klosterneuburg, Austria, 
+43-(0)2243 9000, twist@ist.ac.at, for commercial licensing opportunities.